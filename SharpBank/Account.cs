﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private readonly int accountType;
        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }


        // RMF 07/05/2019 Interest rates should accrue daily (incl. weekends), rates above are per-annum
        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount *  (Math.Pow(1 + 0.001 / 365, 365) - 1);
                    else
                        return 1000 * (Math.Pow(1 + 0.001 / 365, 365) - 1)  + (amount - 1000) * (Math.Pow(1 + 0.002 / 365, 365) - 1) ;

                // case SUPER_SAVINGS:
                //     if (amount <= 4000)
                //         return 20;

                case MAXI_SAVINGS:


                    // RMF 07/05/2019  Change Maxi-Savings accounts to have an interest rate of 5%
                    // assuming no withdrawals in the past 10 days otherwise 0.1%

                    if (this.transactions.Count(x => x.transactionDate >= DateProvider.GetInstance().TenDaysAgo()
                                 && x.amount < 0  ) > 0)
                    {
                        return amount * (Math.Pow(1 + 0.001 / 365, 365) - 1) ;
                    }
                    else
                    {
                        return amount * (Math.Pow(1 + 0.05 / 365, 365) - 1);
                       
                    }            
                default:
                    return amount * (Math.Pow(1 + 0.001 / 365, 365) - 1);
            }
        }

        public double SumTransactions()
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }

        public int GetAccountType()
        {
            return accountType;
        }

    }
}
